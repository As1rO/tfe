import "../assets/styles/app.scss";
import "../assets/styles/styles.scss";
import { addEventLiistenerButton } from "../assets/javascripts/carouselle.js";
import { slides } from "../assets/javascripts/carouselle.js";
import { saveI } from "../assets/javascripts/carouselle.js";
import { setPositionEnd } from "../assets/javascripts/carouselle.js";
import { pxDifTab } from "../assets/javascripts/carouselle.js";
import { total } from "../assets/javascripts/carouselle.js";
import { slider } from "../assets/javascripts/carouselle.js";
import "../assets/javascripts/loader.js";
import "../assets/javascripts/switchCam.js";
import "../assets/javascripts/popup";
import "../assets/javascripts/carouselle.js";
import * as p5 from "../assets/javascripts/lib/p5.min.js";
const name = require("../assets/javascripts/json/artist.json");

////  global variables

let toggleShadders = false;
let shaders = {};
var cam;

var shaderLayer;
let curentShader;
let shadersTab = [];
let curentActive = 0;
var constraints;
let switchFlag = false;
const now = new Date();
let curentDate;
let curentSeconds;
let curentSeconds2;
let pos;
let boolen = true;
let localBoolen = false;
var compteur = 7;

let camShader;

let numLayers = 60;
let layers = [];
let index1 = 0;
let index2 = numLayers / 3; // 30
let index3 = (numLayers / 3) * 2; // 60

var saturation = "50%";
var luminance = "50%";
var tint = 50;
var pixel = 500;
var frequence = 10;
var amplitude = 0.0;
var tab = [50, "50%", "50%"];
var conversionRgb;
var conversionGls = [0.25, 0.73, 0.75];
var titleFilter = "custom";

const animVideo = document.querySelector(".animSvg");
const shareSave = document.querySelector(".button--dl");
const shareCancel = document.querySelector(".cancel");
const shareContainer = document.querySelector(".container-share");
const canvas = document.querySelector("canvas");
var buttonAnimals = document.querySelectorAll(".animals");
const animation = document.querySelector("#animation");
const buttonCancel = document.querySelector("#reset");

const informationButton = document.querySelector(".information");
const switchButton = document.querySelector("#switch");
const titleInput = document.querySelector(".titleInput");

const video = document.querySelector("video");
const inputColor = document.querySelectorAll(".inputColor");
var currentButton;
localStorage.setItem("loglevel:webpack-dev-server", "INFO");

///
////////////////////////////////////////////////
//Select All buttons filters
////////////////////////////////////////////////
///

buttonAnimals.forEach((element, index) => {
  if (window.devicePixelRatio === 1) {
    element.style.backgroundImage = `url("${name[index].bgImage}.png")`;
  } else {
    element.style.backgroundImage = `url("${name[index].bgImage}@2x.png")`;
  }
  element.addEventListener("click", (event) => {
    for (let i = 0; i < buttonAnimals.length; i++) {
      buttonAnimals[i].classList.remove("isActive");
    }
    curentActive = index;

    element.classList.toggle("isActive");
    toggleShadders = true;
  });
});

//// difference between normal filter and custom filter
//// visulaliser = preview of custom filter before saving or cancel

const information = document.querySelector(".information");
const containerInfo = document.querySelector(".info");
const boddy = document.querySelector("body");
const buttonClose = document.querySelector(".button--close");

var visualiser = false;
information.addEventListener("click", (e) => {
  //// normal filter
  if (currentButton > 0 && currentButton < 7) {
    dataString();

    boddy.classList.remove("overflow");
    containerInfo.classList.remove("display");
  }
  //// custom filter
  if (currentButton >= 7) {
    customFilterCreeAdd();

    let key = JSON.parse(localStorage.getItem(currentButton));
    visualiser = true;
    pixel = `${key.pixelKey}`;
    frequence = `${key.frequenceKey}`;
    amplitude = `${key.amplitudeKey}`;
    tab = [50, "50%", "50%"];
    conversionRgb;
    conversionGls = [
      `${key.conversionGlsKey0}`,
      `${key.conversionGlsKey1}`,
      `${key.conversionGlsKey2}`,
    ];
    titleFilter = `${key.titleKey}`;
    titleInput.value = `${key.titleKey}`;

    //// applied parameters for each input
    inputColor.forEach((element) => {
      let id = element.id;

      if (id === "tint") {
        element.value = `${key.tabKey0}`;
      }
      if (id === "saturation") {
        element.value = `${key.tabKey1.slice(0, -1)}`;
      }
      if (id === "lumi") {
        element.value = `${key.tabKey2.slice(0, -1)}`;
      }
      if (id === "frequence") {
        element.value = `${key.frequenceKey}`;
      }
      if (id === "amplitude") {
        element.value = `${key.amplitudeKey * 50}`;
      }

      if (id === "pixel") {
        if (key.pixelKey > 44) {
          element.value = `${key.pixelKey / 100}`;
        } else {
          element.value = `${key.pixelKey}`;
        }
      }
    });

    //// saving new parameters , and pushing into localstorage

    buttonCustomSave.addEventListener("click", buttonSaving);

    //// cancel new parameters , and restore precedent parameters
    buttonCustomCancel.addEventListener("click", (e) => {
      pixel = `${key.pixelKey}`;
      frequence = `${key.frequenceKey}`;
      amplitude = `${key.amplitudeKey}`;
      tab = [50, "50%", "50%"];
      conversionRgb;
      conversionGls = [
        `${key.conversionGlsKey0}`,
        `${key.conversionGlsKey1}`,
        `${key.conversionGlsKey2}`,
      ];

      visualiser = false;
      buttonCustomSave.removeEventListener("click", addNewButton);
      customFilterCreeRemove();
    });
    filterPersoRemove();
  }
});

function buttonSaving() {
  visualiser = false;
  let obj = {
    conversionGlsKey0: `${conversionGls[0]}`,
    conversionGlsKey1: `${conversionGls[1]}`,
    conversionGlsKey2: `${conversionGls[2]}`,
    pixelKey: `${pixel}`,
    frequenceKey: `${frequence}`,
    amplitudeKey: `${amplitude}`,
    compteurKey: `${compteur}`,
    tabKey0: `${tab[0]}`,
    tabKey1: `${tab[1]}`,
    tabKey2: `${tab[2]}`,
    titleKey: `${titleFilter}`,
  };

  localStorage.setItem(currentButton, JSON.stringify(obj));
  customFilterCreeRemove();
  let nameEl = document.querySelector(".name__el--change");
  nameEl.innerHTML = titleFilter;

  buttonCustomSave.removeEventListener("click", buttonSaving);
}

buttonClose.addEventListener("click", (e) => {
  containerInfo.classList.add("display");
  boddy.classList.add("overflow");
});

const customFilterButtonRemove = document.querySelector(
  ".button-custom--supprimer"
);
let compteurValue = 7;

///
////////////////////////////////////////////////
//image retina & copy for each author
////////////////////////////////////////////////
///

function dataString() {
  ///// copy for each author
  let CurrentVariable = 0;
  buttonAnimals.forEach((element, index) => {
    if (element.classList.contains("camera--big")) {
      CurrentVariable = index;
    }
  });

  const dataString = document.querySelectorAll("[data-key]");
  const citationContainer = document.querySelector(".citation-container");

  citationContainer.setAttribute(
    "class",
    `citation-container citation-container--${name[CurrentVariable].surname}`
  );
  ///// retinafor each author
  dataString.forEach((element) => {
    let dataKey = element.getAttribute("data-key");
    if (window.devicePixelRatio === 1) {
      if (element.attributes.src) {
        return element.setAttribute(
          "src",
          `${name[CurrentVariable][dataKey]}.jpg`
        );
      }
    }
    if (window.devicePixelRatio > 1) {
      if (element.attributes.src) {
        return element.setAttribute(
          "src",
          `${name[CurrentVariable][dataKey]}@2x.jpg`
        );
      }
    }

    if (element.attributes.href) {
      return element.setAttribute("href", name[CurrentVariable][dataKey]);
    } else {
      return (element.innerHTML = name[CurrentVariable][dataKey]);
    }
  });
}

////  utility function
function disablebutton(button) {
  button.disabled = true;
}

function Activatebutton(button) {
  button.disabled = false;
}

//// p5js initialization
const sketch = function (p) {
  p.preload = function () {
    preloadF(p);
  };
  p.setup = function () {
    setupF(p);
  };
  p.draw = function () {
    drawF(p);
  };

  p.windowResized = function () {
    p.resizeCanvas(p.windowWidth, p.windowHeight);
  };
};

//// load glsl shaders

function preloadF(p) {
  shaders = {
    reset: p.loadShader(
      "./assets/javascripts/shaders/reset.vert",
      "./assets/javascripts/shaders/reset.frag"
    ),
    effect: p.loadShader(
      "./assets/javascripts/shaders/effect.vert",
      "./assets/javascripts/shaders/effect.frag"
    ),
    webcam: p.loadShader(
      "./assets/javascripts/shaders/webcam.vert",
      "./assets/javascripts/shaders/webcam.frag"
    ),
    treshold: p.loadShader(
      "./assets/javascripts/shaders/treshold.vert",
      "./assets/javascripts/shaders/treshold.frag"
    ),

    blue: p.loadShader(
      "./assets/javascripts/shaders/blue.vert",
      "./assets/javascripts/shaders/blue.frag"
    ),
    munch: p.loadShader(
      "./assets/javascripts/shaders/munch.vert",
      "./assets/javascripts/shaders/munch.frag"
    ),
    delay: p.loadShader(
      "./assets/javascripts/shaders/delay.vert",
      "./assets/javascripts/shaders/delay.frag"
    ),
    custom: p.loadShader(
      "./assets/javascripts/shaders/custom.vert",
      "./assets/javascripts/shaders/custom.frag"
    ),
  };
  shadersTab = Object.values(shaders);
}

//// p5js setup start recording webcam

function setupF(p) {
  let cnv = p.createCanvas(p.windowWidth, p.windowHeight, p.WEBGL);
  cnv.position(0, 0, "fixed");
  p.noStroke();
  constraints = {
    video: {
      facingMode: "environment",
    },
  };

  curentShader = shadersTab[0];
  cam = p.createCapture(constraints, p.VIDEO);
  cam.size(p.windowWidth, p.windowHeight);

  cam.hide();
  toShare();
  switchCam(p);
}

///
////////////////////////////////////////////////
// differentiate between a picture and a video // using Date
////////////////////////////////////////////////
///

function animSvg() {
  buttonAnimals = document.querySelectorAll(".animals");
  buttonAnimals.forEach((element) => {
    var timeout;

    element.addEventListener(
      "touchstart",
      (e) => {
        curentDate = new Date();
        curentSeconds = curentDate.getSeconds();
        timeout = setTimeout(function () {
          if (element.classList.contains("camera--big")) {
            videoDl();

            animVideo.classList.add("animVideo");
          }
        }, 1100);

        element.addEventListener(
          "touchmove",
          (event) => {
            boolen = false;
            clearTimeout(timeout);
          },
          { passive: true }
        );
      },
      { passive: true }
    );

    element.addEventListener("touchend", (e) => {
      curentDate = new Date();
      curentSeconds2 = curentDate.getSeconds();

      if (
        curentSeconds === curentSeconds2 &&
        boolen === true &&
        element.classList.contains("camera--big")
      ) {
        download();
      }

      boolen = true;
      clearTimeout(timeout);
      currentButton = setPositionEnd(saveI);

      customFilterCree(saveI);
      appliedShader();
      disableFirstInfoFilter();
      textSwap();
    });
  });
}

animSvg();

///
////////////////////////////////////////////////
// Shaders Choice // Background image change
////////////////////////////////////////////////
///

function textSwap() {
  buttonAnimals.forEach((element, index) => {
    if (element.classList.contains("camera--big")) {
      changeEyes(index);
    }
  });
}

function appliedShader() {
  buttonAnimals.forEach((element, index) => {
    if (element.classList.contains("camera--big") && currentButton >= 7) {
      return (curentShader = shadersTab[7]);
    }
    if (element.classList.contains("camera--big")) {
      return (curentShader = shadersTab[index]);
    }
  });
}

///
////////////////////////////////////////////////
// swaping copy filter
////////////////////////////////////////////////
///

function changeEyes(index) {
  let nameEl = document.querySelector(".name__el--change");
  nameEl.style.animation = "0.6s test ease";

  //// current shaders
  if (index < 7 || index === compteur) {
    if (index === compteur) {
    } else {
      nameEl.innerHTML = name[index].surname;
      let removeAnimation = setTimeout(function () {
        nameEl.style.animation = "";
      }, 600);
    }
  }
  //// custom shaders
  else {
    if (localBoolen === true && localStorage.getItem("7") != null) {
      let key = JSON.parse(localStorage.getItem(`${currentButton}`));
      nameEl.innerHTML = `${key.titleKey}`;
    } else {
      let key = JSON.parse(localStorage.getItem(`${currentButton}`));
      nameEl.innerHTML = `${key.titleKey}`;
    }
  }
}

///
////////////////////////////////////////////////
//Pause Interface after 8s
////////////////////////////////////////////////
///

var timeoutInterface;
pauseInterface();

/// pause interface
function pauseInterface() {
  timeoutInterface = timeoutInterface = setTimeout(function () {
    informationButton.classList.add("display--opacity");
    switchButton.classList.add("display--opacity");

    for (let i = 0; i < buttonAnimals.length; i++) {
      if (!buttonAnimals[i].classList.contains("camera--big")) {
        buttonAnimals[i].classList.add("display--opacity");
      }
    }
  }, 8000);
}
let body = document.querySelector("main");
body.addEventListener(
  "touchstart",
  (e) => {
    clearPauseInterface();
  },
  { passive: true }
);

/// remove pause interface
function clearPauseInterface() {
  clearTimeout(timeoutInterface);
  informationButton.classList.remove("display--opacity");
  switchButton.classList.remove("display--opacity");
  for (let i = 0; i < buttonAnimals.length; i++) {
    if (!buttonAnimals[i].classList.contains("camera--big")) {
      buttonAnimals[i].classList.remove("display--opacity");
    }
  }
  pauseInterface();
}

/// filter "current working"
function worked() {
  let woredContainer = document.querySelector(".worked");
  if (shadersTab[6] === curentShader) {
    woredContainer.classList.remove("display");
  } else {
    woredContainer.classList.add("display");
  }
}
information.disabled = true;
function disableFirstInfoFilter() {
  if (shadersTab[0] === curentShader) {
    information.disabled = true;
  } else {
    information.disabled = false;
  }
}

///
////////////////////////////////////////////////
// input color when user customise his filter
////////////////////////////////////////////////
///

function inputColorEvent() {
  titleInput.addEventListener("keyup", (event) => {
    titleFilter = titleInput.value;
  });
  inputColor.forEach((input, index) => {
    let id = input.id;

    input.addEventListener("input", (event) => {
      conversionGls = [];
      if (id === "tint") {
        tint = input.value;
      }
      if (id === "saturation") {
        saturation = `${input.value}%`;
      }
      if (id === "lumi") {
        luminance = `${input.value}%`;
      }
      if (id === "frequence") {
        frequence = `${input.value}`;
      }
      if (id === "amplitude") {
        amplitude = `${input.value / 50}`;
      }

      if (id === "pixel") {
        if (input.value > 44) {
          pixel = `${input.value * 100}.0`;
        } else {
          pixel = `${input.value}.0`;
        }
      }
      tab = [tint, saturation, luminance];

      conversionRgb = hslToRgb(`hsl(${tab[0]}, ${tab[1]}, ${tab[2]})`);
      for (const value of conversionRgb) {
        conversionGls.push(Math.round((value / 255) * 100) / 100);
      }
      return conversionGls;
    });
  });
}
inputColorEvent();

///
////////////////////////////////////////////////
//hsl to rgb  https://www.html-code-generator.com/javascript/color-converter-script
////////////////////////////////////////////////
///

function hslToRgb(hsl) {
  hsl = hsl.match(
    /^hsla?\(\s?(\d+)(?:deg)?,?\s(\d+)%,?\s(\d+)%,?\s?(?:\/\s?\d+%|\s+[\d+]?\.?\d+)?\)$/i
  );
  if (!hsl) {
    return null;
  }
  let h = hsl[1];
  let s = hsl[2];
  let l = hsl[3];
  let C;
  let X;
  let b;
  let g;
  let r;
  let m;
  s /= 100;
  l /= 100;
  C = (1 - Math.abs(2 * l - 1)) * s;
  var hue = h / 60;
  X = C * (1 - Math.abs((hue % 2) - 1));
  r = g = b = 0;
  if (hue >= 0 && hue < 1) {
    r = C;
    g = X;
  } else if (hue >= 1 && hue < 2) {
    r = X;
    g = C;
  } else if (hue >= 2 && hue < 3) {
    g = C;
    b = X;
  } else if (hue >= 3 && hue < 4) {
    g = X;
    b = C;
  } else if (hue >= 4 && hue < 5) {
    r = X;
    b = C;
  } else {
    r = C;
    b = X;
  }
  m = l - C / 2;
  r += m;
  g += m;
  b += m;
  r *= 255.0;
  g *= 255.0;
  b *= 255.0;
  return [Math.round(r), Math.round(g), Math.round(b)];
}

///
////////////////////////////////////////////////
//filter Custom
////////////////////////////////////////////////
///

const customFilter = document.querySelector(".customFilter");
const containerButton = document.querySelector(".containerButton");
const circle = document.querySelectorAll(".circle");
const containerName = document.querySelector(".container-name");
const flexButton = document.querySelector(".flexButton");

let buttonCustomSave = document.querySelector(".button-custom--save");
let buttonCustomCancel = document.querySelector(".button-custom--cancel");

//// custom filter

function customFilterCree(saveI) {
  if (currentButton === compteur || testee === true) {
    titleInput.value = "custom";
    inputColor.forEach((element) => {
      let id = element.id;

      if (id === "tint") {
        element.value = "180";
      }
      if (id === "saturation") {
        element.value = "50";
      }
      if (id === "lumi") {
        element.value = "50";
      }
      if (id === "frequence") {
        element.value = "10";
      }
      if (id === "amplitude") {
        element.value = "0.0";
      }

      if (id === "pixel") {
        element.value = "500";
      }
    });

    //// restore value when user create a new filter

    saturation = "50%";
    luminance = "50%";
    tint = 50;
    pixel = 500;
    frequence = 10;
    amplitude = 0.0;
    tab = [50, "50%", "50%"];
    conversionRgb;
    conversionGls = [0.25, 0.73, 0.75];
    customFilterCreeAdd();

    buttonCustomSave.addEventListener("click", addNewButton);
    buttonCustomCancel.addEventListener("click", (e) => {
      buttonCustomSave.removeEventListener("click", addNewButton);
      customFilterCreeRemove();
    });

    slider.style.transform = `translate(${total + 57}px)`;
  } else {
    customFilterCreeRemove();
  }
}
let testee = false;

//// remove  custom filter

function filterPersoRemove() {
  customFilterButtonRemove.addEventListener("click", customRemoveEvent);
}

//// remove custom filter
function customRemoveEvent() {
  let customFilterButton = document.querySelectorAll(".customButton");
  customFilterButton.forEach((element) => {
    if (element.classList.contains("camera--big")) {
      if (currentButton === 7) {
        curentShader = shadersTab[currentButton - 1];
        let nameEl = document.querySelector(".name__el--change");

        nameEl.innerHTML = "warhol";
      } else {
        curentShader = shadersTab[7];

        let key = JSON.parse(localStorage.getItem(currentButton - 1));

        pixel = `${key.pixelKey}`;
        frequence = `${key.frequenceKey}`;
        amplitude = `${key.amplitudeKey}`;
        tab = [50, "50%", "50%"];
        conversionRgb;
        conversionGls = [
          `${key.conversionGlsKey0}`,
          `${key.conversionGlsKey1}`,
          `${key.conversionGlsKey2}`,
        ];
        let nameEl = document.querySelector(".name__el--change");
        titleFilter = `${key.titleKey}`;
        nameEl.innerHTML = key.titleKey;
      }

      visualiser = false;
      element.classList.add("button-removeAnimation");
      let removeAnimation = setTimeout(function () {
        element.remove();

        localStorage.removeItem(`${currentButton}`);

        for (let cle of Object.keys(localStorage)) {
          let key = localStorage.getItem(cle);

          if (key != "INFO") {
            localStorage.setItem(`${compteurValue}`, key);
            compteurValue++;
          }
        }

        slides.splice(currentButton, 1);
        buttonAnimals = document.querySelectorAll(".animals");
        compteur--;
        slider.style.transform = `translate(${total + 57}px)`;
        curentShader = shadersTab[compteur - 1];
      }, 500);
      customFilterButtonRemove.removeEventListener("click", customRemoveEvent);
      customFilterCreeRemove();
    }
  });
}

/// create new custom buttons

function createButtonAnimal() {
  buttonAnimals.forEach((element) => {
    element.classList.remove("camera--big");
  });
  let buttonAnimal = document.createElement("button");
  buttonAnimal.classList.add("animals");
  buttonAnimal.classList.add("camera--trigger");
  buttonAnimal.classList.add("camera--big");
  buttonAnimal.classList.add("customButton");

  flexButton.insertBefore(buttonAnimal, flexButton.lastElementChild);

  buttonAnimals = document.querySelectorAll(".animals");
  slides.splice(slides.length - 1, 0, buttonAnimal);
  addEventLiistenerButton(slides);

  animSvg();
}

//// adding new button

function addNewButton() {
  customFilterCreeRemove();
  createButtonAnimal();

  let obj = {
    conversionGlsKey0: `${conversionGls[0]}`,
    conversionGlsKey1: `${conversionGls[1]}`,
    conversionGlsKey2: `${conversionGls[2]}`,
    pixelKey: `${pixel}`,
    frequenceKey: `${frequence}`,
    amplitudeKey: `${amplitude}`,
    compteurKey: `${compteur}`,
    tabKey0: `${tab[0]}`,
    tabKey1: `${tab[1]}`,
    tabKey2: `${tab[2]}`,
    titleKey: `${titleFilter}`,
  };
  let nameEl = document.querySelector(".name__el--change");
  nameEl.innerHTML = titleFilter;

  localStorage.setItem(compteur, JSON.stringify(obj));
  textSwap();
  inputColorEvent();

  compteur++;

  buttonCustomSave.removeEventListener("click", addNewButton);
}

//// fonction to reveal element

function customFilterCreeAdd() {
  customFilter.classList.remove("display");
  informationButton.classList.add("display");
  switchButton.classList.add("display");
  containerButton.classList.add("display");
  circle.forEach((element) => {
    element.classList.add("display");
  });
  containerName.classList.add("display");
}

//// fonction to hide element

function customFilterCreeRemove() {
  customFilter.classList.add("display");
  informationButton.classList.remove("display");
  switchButton.classList.remove("display");
  containerButton.classList.remove("display");
  circle.forEach((element) => {
    element.classList.remove("display");
  });
  containerName.classList.remove("display");
}

//// checking local storage when the dom is loaded

document.addEventListener("DOMContentLoaded", (e) => {
  if (localStorage.getItem("7") != null) {
    for (let i = 0; i < localStorage.length - 1; i++) {
      localBoolen = true;
      createButtonAnimal();
      compteur++;
    }
  } else {
    localBoolen = false;
  }
});

//// p5js draw fonction and custom variable for each filters

function drawF(p) {
  if (shadersTab[5] === curentShader) {
    p.shader(curentShader);
    curentShader.setUniform("tex0", cam);
    curentShader.setUniform("time", p.frameCount * 0.01);
    curentShader.setUniform("mouseX", 0.5);

    let freq = 10;
    let amp = 0.15;

    curentShader.setUniform("frequency", freq);
    curentShader.setUniform("amplitude", amp);
    p.rect(0, 0, p.width, p.height);
  }
  if (shadersTab[2] === curentShader) {
    p.shader(curentShader);
    curentShader.setUniform("tex0", cam);
    curentShader.setUniform("tex2", cam);
    p.rect(0, 0, p.width, p.height);
  }
  if (shadersTab[3] === curentShader) {
    p.shader(curentShader);
    curentShader.setUniform("mouseX", 0.5);
    p.rect(0, 0, p.width, p.height);
  }

  if (shadersTab[6] === curentShader) {
    p.shader(curentShader);
    curentShader.setUniform("tex0", cam);
    curentShader.setUniform("resolution", [p.width, p.height]);
    p.rect(0, 0, p.width, p.height);
  }
  if (shadersTab[7] === curentShader) {
    if (currentButton === compteur || visualiser) {
      p.shader(curentShader);
      curentShader.setUniform("tex0", cam);
      curentShader.setUniform("tored", conversionGls[0]);
      curentShader.setUniform("togreen", conversionGls[1]);
      curentShader.setUniform("toblue", conversionGls[2]);
      curentShader.setUniform("pixel", pixel);
      curentShader.setUniform("time", p.frameCount * 0.01);

      let freq = 10;
      let amp = 0.15;

      curentShader.setUniform("frequency", frequence);
      curentShader.setUniform("amplitude", amplitude);
      p.rect(0, 0, p.width, p.height);
    } else {
      let key = JSON.parse(localStorage.getItem(currentButton));
      p.shader(curentShader);
      curentShader.setUniform("tex0", cam);
      curentShader.setUniform("tored", key.conversionGlsKey0);
      curentShader.setUniform("togreen", key.conversionGlsKey1);
      curentShader.setUniform("toblue", key.conversionGlsKey2);
      curentShader.setUniform("pixel", key.pixelKey);
      curentShader.setUniform("time", p.frameCount * 0.01);

      let freq = 10;
      let amp = 0.15;

      curentShader.setUniform("frequency", key.frequenceKey);
      curentShader.setUniform("amplitude", key.amplitudeKey);
      p.rect(0, 0, p.width, p.height);
    }
  } else {
    p.shader(curentShader);
    curentShader.setUniform("tex0", cam);
    curentShader.setUniform("tex1", shaders.img);
    curentShader.setUniform("resolution", [p.width, p.height]);
    p.rect(0, 0, p.width, p.height);
  }
}

///
////////////////////////////////////////////////
//Download image
////////////////////////////////////////////////
///

function download() {
  let shareSave = document.querySelector(".button--dl");
  let downloadButton = document.querySelector(".button--download");
  let canvas = document.querySelector(".p5Canvas");

  let dataUrl;

  dataUrl = canvas.toDataURL();
  shareContainer.classList.remove("display");
  let img = new Image();
  img.classList.add("shareImg");
  img.src = dataUrl;
  shareContainer.append(img);
  if (dataUrl != "") {
    shareSave.href = dataUrl;
  }

  shareSave.setAttribute(
    "download",
    `picture${now.getFullYear()}-${(now.getMonth() + 1)
      .toString()
      .padStart(2, "0")}-${now.getDay().toString().padStart(2, "0")}--${now
      .getHours()
      .toString()
      .padStart(2, "0")}-${now.getMinutes().toString().padStart(2, "0")}-${now
      .getSeconds()
      .toString()
      .padStart(2, "0")}.png`
  );

  shareSave.addEventListener("click", (e) => {
    let imgRemove = document.querySelector(".shareImg");
    shareContainer.classList.add("display");
    imgRemove.remove();
    Activatebutton(information);
  });

  shareCancel.addEventListener("click", (e) => {
    let imgRemove = document.querySelector(".shareImg");
    shareContainer.classList.add("display");
    if (imgRemove != null) {
      imgRemove.remove();
      Activatebutton(information);
    }
  });
}

///
////////////////////////////////////////////////
//Download Video
////////////////////////////////////////////////
///

function videoDl() {
  let boolean = false;
  let canvas = document.querySelector(".p5Canvas");
  let buttonVideo = document.querySelector(".camera--big");
  const mimeType = "video/webm";
  let chunks = [];
  var stream = canvas.captureStream(25);
  var options = { mimeType: "video/webm; codecs=vp9" };
  const recorder = new MediaRecorder(stream, options);

  recorder.addEventListener("dataavailable", (event) => {
    if (typeof event.data === "undefined") return;
    if (event.data.size === 0) return;
    chunks.push(event.data);
  });
  recorder.addEventListener("stop", () => {
    const recording = new Blob(chunks, {
      type: "video/webm",
    });

    renderRecording(recording);
    chunks = [];
  });

  recorder.start();
  let timeout = (timeout = setTimeout(function () {
    if (boolean === false) {
      recorder.stop();
      shareContainer.classList.remove("display");
      animVideo.classList.remove("animVideo");
      boolean = true;
    }
  }, 5000));
  buttonVideo.addEventListener("touchend", (e) => {
    if (boolean === false) {
      recorder.stop();
      boolean = true;
      shareContainer.classList.remove("display");
      animVideo.classList.remove("animVideo");
      disablebutton(information);
    }
  });
}

//// compiling video

function renderRecording(blob) {
  const blobUrl = URL.createObjectURL(blob);
  const video = document.createElement("video");
  shareSave.setAttribute("href", blobUrl);

  shareSave.setAttribute(
    "download",
    `recording-${now.getFullYear()}-${(now.getMonth() + 1)
      .toString()
      .padStart(2, "0")}-${now.getDay().toString().padStart(2, "0")}--${now
      .getHours()
      .toString()
      .padStart(2, "0")}-${now.getMinutes().toString().padStart(2, "0")}-${now
      .getSeconds()
      .toString()
      .padStart(2, "0")}.webm`
  );
  shareSave.addEventListener("click", saveVideo);
  shareCancel.addEventListener("click", saveCancel);

  function saveVideo() {
    video.remove();
    shareSave.setAttribute("download", "");
    shareContainer.classList.add("display");
  }
  function saveCancel() {
    video.remove();
    shareContainer.classList.add("display");
  }

  video.setAttribute("src", blobUrl);
  video.setAttribute("controls", "controls");
  video.setAttribute(
    "controlsList",
    "nodownload nofullscreen noremoteplayback "
  );
  video.classList.add("shareImg");
  shareContainer.append(video);
}

///
////////////////////////////////////////////////
//Share image/video
////////////////////////////////////////////////
///

function toShare() {
  let linksocial = document.querySelectorAll(".linksocial");

  let postUrl = encodeURI(document.location.href);

  let postTitle = encodeURI("hi, this is my app ");

  linksocial.forEach((element) => {
    let classe = element.getAttribute("data-social");

    if (classe === "facebook") {
      element.setAttribute(
        "href",
        `https://www.facebook.com/sharer.php?u=olivier.denis/test/tfe`
      );
    }
    if (classe === "twitter") {
      element.setAttribute(
        "href",
        `https://twitter.com/share?url=${postUrl}&text=${postTitle}`
      );
    }
    if (classe === "instagram") {
      element.setAttribute("href", `https://www.instagram.com/?url=${postUrl}`);
    }
  });
}

///
////////////////////////////////////////////////
//Switch caméra front and back
////////////////////////////////////////////////
///

function switchCam(p) {
  const switchCamera = document.querySelector("#switch");
  switchCamera.addEventListener("click", () => {
    switchFlag = !switchFlag;

    if (switchFlag == true) {
      cam.remove();
      constraints.video.facingMode = "user";
    } else {
      cam.remove();
      constraints.video.facingMode = "environment";
    }
    cam = p.createCapture(constraints, p.VIDEO);
    cam.size(p.windowWidth, p.windowHeight);

    cam.hide();
  });
}

function newP5() {
  new p5(sketch, "animation");
}
newP5();

export { buttonAnimals, curentActive, constraints, cam };
