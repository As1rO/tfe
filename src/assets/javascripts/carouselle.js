var brush = new Audio("./assets/sounds/brush2.mp3");
let total = 0;
var slider;
var saveI;

function addEventLiistenerButton(slides) {
  slides.forEach((slide, index) => {
    slide.addEventListener("dragstart", (event) => event.preventDefault());

    slide.addEventListener("touchstart", touchStart(index), { passive: true });
    slide.addEventListener("touchend", touchEnd, { passive: true });
    slide.addEventListener("touchmove", touchMove, { passive: true });
    //
    slide.addEventListener("mousedown", touchStart(index), { passive: true });
    slide.addEventListener("mouseup", touchEnd, { passive: true });
    slide.addEventListener("mouseleave", touchEnd, { passive: true });
    slide.addEventListener("mosemove", touchMove, { passive: true });
  });
}

var slides = Array.from(document.querySelectorAll(".animals"));

slider = document.querySelector(".flexButton");

let isDragging = false;
let startPos = 0;
let currentTranSlate = 0;
let prevTranslate = 0;
let animationID = 0;
let currentIndex = 0;
var pxDifTab = [];
let translateX;
let currentPosition;

var pxDif = slides[3].offsetLeft - slides[2].offsetLeft;

window.addEventListener("resize", (e) => {
  pxDif = slides[3].offsetLeft - slides[2].offsetLeft;
});

addEventLiistenerButton(slides);

window.oncontextmenu = function (event) {
  event.preventDefault();
  event.stopPropagation();
  return false;
};

function touchStart(index) {
  return function (event) {
    currentIndex = index;
    startPos = getPositionX(event);

    isDragging = true;
    animationID = requestAnimationFrame(animation);
    slides;
    slider.classList.add("containerGrabbing");
  };
}

function touchEnd() {
  isDragging = false;
  cancelAnimationFrame(animationID);

  const moveBy = currentTranSlate - prevTranslate;
  if (moveBy < -100 && currentIndex < slides.length - 1) {
    currentIndex += 1;
  }

  if (moveBy > -100 && currentIndex > 0) {
    currentIndex -= 1;
  }

  setPositionEnd();
  slider.classList.remove("containerGrabbing");
  slides.forEach((element) => {});
  brush.play();
}

function touchMove(event) {
  if (isDragging) {
    currentPosition = getPositionX(event);

    currentTranSlate = prevTranslate + currentPosition - startPos;
  }
}

function getPositionX(event) {
  return event.type.includes("mouse") ? event.pageX : event.touches[0].clientX;
}

function animation() {
  setSliderPosition();

  if (isDragging) {
    requestAnimationFrame(animation);
  }
}

function setSliderPosition() {
  translateX = `translate(${currentTranSlate}px)`;
  slider.style.transform = translateX;
}

function setPositionEnd(saveI) {
  pxDifTab = [];

  for (let i = 0; i < slides.length; i++) {
    pxDifTab.push(-pxDif * i);
  }

  let inipxEnd = Math.abs(pxDifTab[0] - currentTranSlate);

  for (let i = 0; i < slides.length; i++) {
    let pxEnd = Math.abs(pxDifTab[i] - currentTranSlate);
    if (pxEnd <= inipxEnd) {
      inipxEnd = pxEnd;
      saveI = i;
      total = pxDifTab[i];
    }
  }

  currentTranSlate = total;
  slides.forEach((element) => {
    element.classList.remove("camera--big");
  });

  if (slides[saveI]) {
    slides[saveI].classList.add("camera--big");
  } else {
    slides[0].classList.add("camera--big");
  }

  slider.style.transition = "transform 0.1s ease";
  slider.style.transform = `translate(${total}px)`;
  prevTranslate = currentTranSlate;

  return saveI;
}

export {
  total,
  addEventLiistenerButton,
  slides,
  setPositionEnd,
  saveI,
  pxDifTab,
  slider,
};
