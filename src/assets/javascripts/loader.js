var timing;

const loader = document.querySelector(".loader");
const path = document.querySelector(".logo-path3");
const path2 = document.querySelector(".logo-path1");
const boxAnim = document.querySelector(".logo-path1");

function loaded() {
  timing = setTimeout(removeLoder, 3000);
}

function loadedAnim() {
  loader.remove();
}

function removeLoder() {
  timing = setTimeout(loadedAnim, 500);
  loader.classList.add("loadingAnim");
}
if (document.body.clientWidth < 550) {
  loaded();
} else {
  window.addEventListener("resize", (e) => {
    if (document.body.clientWidth < 550) {
      loaded();
    }
  });
}
