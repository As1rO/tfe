precision mediump float;

varying vec2 vTexCoord;


uniform sampler2D tex0;

void main() {

  vec2 uv = vTexCoord;

  uv = 1.0 - uv;


  vec4 cam = texture2D(tex0, uv);

  float blue = cam.b *1.2;

  vec4 color = vec4(0.0, 0.0, blue, 1.0);
  gl_FragColor = color;


}