precision mediump float;

varying vec2 vTexCoord;


uniform sampler2D tex0;
uniform sampler2D tex2;
uniform float tored;
uniform float togreen;
uniform float toblue;
uniform float pixel;
uniform float time;
uniform float frequency;
uniform float amplitude;

float rectangle(in vec2 st, in vec2 origin, in vec2 dimensions) {
    vec2 bl = step(origin, st);
    float pct = bl.x * bl.y;
    vec2 tr = step(1.0 - origin - dimensions, 1.0 - st);
    pct *= tr.x * tr.y;
    return pct;
}


void main() {

  vec2 uv = vTexCoord;

  uv = 1.0 - uv;
   float sineWave = sin(uv.y * frequency + time) * amplitude;

  vec2 distort = vec2(sineWave, 0.0);



  float tiles = pixel;
  uv = uv * tiles;

  uv = floor(uv);

  uv = uv / tiles;


  vec4 cam = texture2D(tex0, uv + distort);


  float red = cam.r *tored;
  float green = cam.g *togreen;
  float blue = cam.b *toblue;

  vec4 color = vec4(red,green,blue,1.0);
  gl_FragColor = color;


}