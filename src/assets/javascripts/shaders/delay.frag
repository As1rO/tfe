precision mediump float;

// lets grab texcoords just for fun
varying vec2 vTexCoord;

// our texture coming from p5
uniform sampler2D tex0;
uniform vec2 resolution;

void main() {

  vec2 uv = vTexCoord;

  uv = 1.0 - uv;


  vec2 pixelSize = vec2(4.0) / resolution;


  vec2 offset = pixelSize * 15.0;

  vec4 rTex = texture2D(tex0, uv - offset);
  vec4 gTex = texture2D(tex0, uv);
  vec4 bTex = texture2D(tex0, uv + offset);

  vec4 color = vec4(rTex.r , gTex.g, bTex.b, 1.0);

  gl_FragColor = color;
}