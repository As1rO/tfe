precision mediump float;

varying vec2 vTexCoord;


uniform sampler2D tex0;

float rectangle(in vec2 st, in vec2 origin, in vec2 dimensions) {
    vec2 bl = step(origin, st);
    float pct = bl.x * bl.y;
    vec2 tr = step(1.0 - origin - dimensions, 1.0 - st);
    pct *= tr.x * tr.y;
    return pct;
}

void main() {

  vec2 uv = vTexCoord;

  uv = 1.0 - uv;

  float tiles = 10.0;
  uv = uv * tiles;

  uv = floor(uv);

  uv = uv / tiles;


 vec4 cam = texture2D(tex0, uv);
  vec4 cam2 = texture2D(tex0, uv);

  float blue = cam.b;
  float red = cam2.r;

  vec4 color = vec4(red, cam.g, blue, 1.0);
  gl_FragColor = color;


}