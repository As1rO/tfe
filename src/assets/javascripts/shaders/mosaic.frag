precision mediump float;


varying vec2 vTexCoord;


uniform sampler2D tex0;
uniform vec2 resolution;


float amt = 0.0;
float squares =  7.5;

void main() {
  float aspect = resolution.x / resolution.y;
  float offset = amt;

  vec2 uv = vTexCoord;


  uv.y = 1.0 - uv.y;


  vec2 tc = uv;


  uv -= 0.5;

  /
  uv.x *= aspect;


  vec2 tile = fract(uv * squares);


  vec4 tex = texture2D(tex0, tc + tile - offset);


  gl_FragColor = tex;
}