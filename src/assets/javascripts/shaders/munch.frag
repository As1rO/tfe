precision mediump float;


varying vec2 vTexCoord;


uniform sampler2D tex0;
uniform float time;
uniform float frequency;
uniform float amplitude;

float luma(vec3 color) {
  return dot(color, vec3(0.299, 0.587, 0.114));
}

void main() {

  vec2 uv = vTexCoord;

  uv = 1.0 - uv;

  float sineWave = sin(uv.y * frequency + time) * amplitude;

  vec2 distort = vec2(sineWave, 0.0);

  vec4 tex = texture2D(tex0, uv + distort);

  float gray = luma(tex.rgb);

  gl_FragColor = vec4(gray, gray, gray, 1.0);
}