// import takepicture from "./camera.JS";
import "./assets/styles/styles.scss";
import "./index.scss";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import Swiper, { Navigation, Pagination, Autoplay, EffectFade } from "swiper";
import "swiper/swiper-bundle.css";

//gsap

gsap.registerPlugin(ScrollTrigger);

if (window.matchMedia("(max-width: 550px)").matches) {
  var start = "top 60%";
} else {
  var start = "top 60%";
}
const paramètres = {
  duration: 1.4,
  ease: "power1",
  markers: false,
  start: start,
  end: "+=200",
};

var nbrTime = true;
function animateFrom(elem, direction) {
  if (elem.classList.contains("disable")) {
    null;
  } else {
    direction = direction | 1;

    var x = 0,
      y = direction * 100;
    if (elem.classList.contains("gs_reveal")) {
      elem.classList.add("disable");
    }
    if (elem.classList.contains(" gs_reveal_fromLeft")) {
      x = 100;
      y = 0;
    } else if (elem.classList.contains("gs_reveal_fromRight")) {
      x = 100;
      y = 0;
    } else if (elem.classList.contains("gs_reveal_fromtop")) {
      x = 0;
      y = 0;
    }

    gsap.fromTo(
      elem,
      { x: x, y: y, opacity: 0 },
      {
        duration: paramètres.duration,
        x: 0,
        y: 0,
        opacity: 1,
        ease: paramètres.ease,
        overwrite: "auto",
      }
    );
  }
}

function hide(elem) {
  gsap.set(elem, { opacity: 0 });
}

document.addEventListener("DOMContentLoaded", function () {
  gsap.registerPlugin(ScrollTrigger);

  gsap.utils.toArray(".gs_reveal").forEach(function (elem) {
    hide(elem);

    ScrollTrigger.create({
      trigger: elem,
      markers: paramètres.markers,
      start: paramètres.start,
      end: paramètres.end,

      onEnter: () => {
        animateFrom(elem);
      },
      onComplete: () => {},
    });
  });
});

const header = document.querySelector("header");
const sectionHeader = document.querySelector(".section--header");
var lastScrollTop = 0;

window.addEventListener("scroll", (e) => {
  var st = window.pageYOffset || document.documentElement.scrollTop;
  if (window.scrollY > window.innerHeight && !(st > lastScrollTop)) {
    sectionHeader.classList.add("section-scroll");
    header.classList.add("header-scroll");
  } else {
    sectionHeader.classList.remove("section-scroll");
    header.classList.remove("header-scroll");
  }
  lastScrollTop = st <= 0 ? 0 : st;
});

const btn = document.querySelector(".link-black");
const root = document.documentElement;

btn.addEventListener("click", (e) => {
  const el = e.target;

  let x = (e.clientX - el.offsetLeft) / el.offsetWidth;
  let y = (e.clientY - el.offsetTop) / el.offsetHeight;
  root.style.setProperty("--wavecoord-x", x);
  root.style.setProperty("--wavecoord-y", y);
});

// swiper.js

Swiper.use([Navigation, Pagination, Autoplay, EffectFade]);
var swiper = new Swiper(".mySwiper", {
  slidesPerView: 4,
  spaceBetween: 40,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
    type: "bullets",
  },
});

var swiper2 = new Swiper(".mySwiper2", {
  slidesPerView: 1,
  spaceBetween: 40,
  fadeEffect: { crossFade: true },
  effect: "fade",
  loop: true,
  autoplay: {
    delay: 2500,
    disableOnInteraction: true,
  },
});

//loader
var timing2;

const loader2 = document.querySelector(".loader-desktop");
const boddy = document.querySelector("body");

function loaded2() {
  boddy.style.overflowY = "hidden";
  timing2 = setTimeout(removeLoder2, 3000);
}

function loadedAnim2() {
  loader2.remove();
}

function removeLoder2() {
  timing2 = setTimeout(loadedAnim2, 500);
  loader2.classList.add("loadingAnim");
  boddy.style.overflowY = "scroll";
}
loaded2();
