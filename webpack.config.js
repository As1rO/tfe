const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
  entry: {
    main: path.join(__dirname, "src/index.js"),
    app: path.join(__dirname, "src/app/app.js"),
    // topbar: path.join(__dirname, "src/assets/javascripts/topbar.js"),
  },
  output: {
    path: path.join(__dirname, "dist"),
    filename: "[name].bundle.js",
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [path.resolve(__dirname, "node_modules")],
        use: ["babel-loader"],
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ["file-loader"],
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: "./src/assets/images/*",
          to: "assets/images/[name].[ext]",
        },

        {
          from: "./src/assets/sounds/*",
          to: "assets/sounds/[name].[ext]",
        },
        {
          from: "./src/assets/javascripts/shaders/*",
          to: "assets/javascripts/shaders/[name].[ext]",
        },
        {
          from: "./src/assets/javascripts/lib/*",
          to: "assets/javascripts/lib/[name].[ext]",
        },
      ],
    }),
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: path.join(__dirname, "./src/index.html"),
      chunks: ["main"],
    }),
    new HtmlWebpackPlugin({
      filename: "app.html",
      template: path.join(__dirname, "./src/app/app.html"),
      chunks: ["app"],
    }),
  ],
  stats: "minimal",
  devtool: "source-map",
  mode: "development",
  devServer: {
    open: false,
    contentBase: path.resolve(__dirname, "./dist"),
    inline: true,
    port: 4000,
  },
};
